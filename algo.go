package main

type sort_fnT func([]int)
type compare_fnT func(int, int) bool

var compare_fn = _ascending

var name_to_algo = map[string]sort_fnT{"bubble": bubble_sort,
	"quick":     quick_sort,
	"insertion": insertion_sort,
	"binsearch": binary_search,
}

func bubble_sort(nums []int) {
	is_done := false
	n := len(nums)
	for !is_done {
		is_done = true
		for i := 0; i < n-1; i++ {
			gen_image(i, n-1, i, i+1, Comparing)
			if compare_fn(nums[i], nums[i+1]) {
				swap(&nums[i], &nums[i+1])
				gen_image(i, n-1, i, i+1, Swapping)
				is_done = false
			}
		}
	}

}

func insertion_sort(nums []int) {
	n := len(nums)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			gen_image(i, n-1, i, j, Comparing)
			if compare_fn(nums[i], nums[j]) {
				gen_image(i, n-1, i, j, Swapping)
				swap(&nums[i], &nums[j])
			}
		}
		isInCorrectPos[i] = true
		//		gen_image(i, n-1, i, i, Done)
	}
}

func quick_sort(nums []int) {
	_quick_sort(nums, 0, len(nums)-1)
}

func _quick_sort(nums []int, l int, r int) {
	if l >= r {
		return
	}
	pivot := nums[r]
	idx := l
	//move all element < pivot to the left of pivot
	for i := l; i < r; i++ {
		gen_image(l, r, idx, i, Comparing)
		if compare_fn(pivot, nums[i]) {
			gen_image(l, r, idx, i, Swapping)
			swap(&nums[i], &nums[idx])
			idx++
		}
	}
	gen_image(l, r, idx, r, Swapping)
	nums[r] = nums[idx]
	nums[idx] = pivot
	isInCorrectPos[idx] = true
	gen_image(l, r, idx, r, Done)
	//all numbers up to pivot are now < pivot
	//sort the  2 sub arrays
	_quick_sort(nums, idx+1, r)
	_quick_sort(nums, l, idx-1)
}

func _descending(a, b int) bool {
	return b > a
}
func _ascending(a, b int) bool {
	return a > b
}

func swap(a, b *int) {
	tmp := *a
	*a = *b
	*b = tmp
}
