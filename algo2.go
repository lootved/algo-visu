package main

import (
	"fmt"
	"image"
	"sort"
)

func binary_search(nums []int) {
	sort.Slice(nums, func(i, j int) bool {
		return compare_fn(nums[j], nums[i])
	})

	target_idx := (2 * len(nums)) / 3
	target := nums[target_idx]
	l := 0
	r := len(nums) - 1
	//	SAVE_FRAMES = true
	gen_image_bin_search(l, r, target_idx)
	m := 0
	for l <= r {
		m = (l + r) / 2
		gen_image_bin_search(l, r, target_idx)
		if nums[m] == target {
			break
		}
		if compare_fn(target, nums[m]) {
			l = m + 1
		} else {
			r = m - 1
		}
	}
	fmt.Println("last index :", m, "has")
}

func gen_image_bin_search(l, r, target_idx int) {
	img := image.NewPaletted(image.Rect(0, 0, WIDTH, HEIGHT), palette)
	images = append(images, img)
	delays = append(delays, GIF_DELAY)
	var val float32
	m := (l + r) / 2

	for j, rect := range rects {
		if m == j {
			if target_idx == m {
				rect.SetColor(255, 255, 255)
			} else {
				rect.SetColor(255, 0, 255)
			}
		} else if j == target_idx {
			rect.SetColor(0, 255, 0)
		} else if j == l || j == r {
			rect.SetColor(0, 255, 255)
		} else {
			rect.SetColor(255, 0, 0)
		}
		val = float32(nums[j]-MIN_VAL) * SCALE
		rect.Set(RECT_OFFSET+j*(RECT_WIDTH+RECT_OFFSET), RECT_OFFSET, RECT_WIDTH, int(val)+RECT_MIN_HEIGHT)
		rect.Draw(img, RECT_HEIGHT_OFFSET/3)
	}

}
