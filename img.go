package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"image/jpeg"
	"os"
	"strconv"
	"strings"
)

var isInCorrectPos []bool

func gen_image(l, r, head, i int, state State) {
	img := image.NewPaletted(image.Rect(0, 0, WIDTH, HEIGHT), palette)
	images = append(images, img)
	delays = append(delays, GIF_DELAY)
	var val float32

	rect := Rect{}
	rect.SetColor(250, 0, 0).Set(RECT_OFFSET+head*(RECT_WIDTH+RECT_OFFSET), RECT_OFFSET, RECT_WIDTH, 50)
	rect.Draw(img, 0)

	rect.SetX(RECT_OFFSET + i*(RECT_WIDTH+RECT_OFFSET))
	if state == Swapping {
		rect.SetColor(0, 255, 255)
	} else {
		rect.SetColor(250, 250, 0)
	}
	rect.Draw(img, 0)
	for j, rect := range rects {
		if state == Swapping && (j == i || j == head) {
			rect.SetColor(0, 255, 255)
		} else if j == l || j == r {
			rect.SetColor(255, 0, 255)
		} else {
			if isInCorrectPos[j] {
				rect.SetColor(0, 255, 0)
			} else {
				rect.SetColor(255, 0, 0)
			}
		}
		val = float32(nums[j]-MIN_VAL) * SCALE
		rect.Set(RECT_OFFSET+j*(RECT_WIDTH+RECT_OFFSET), RECT_OFFSET, RECT_WIDTH, int(val)+RECT_MIN_HEIGHT)
	}

	for _, r := range rects {
		r.Draw(img, RECT_HEIGHT_OFFSET)
	}
}

func save_img() {
	out := "out/" + ALGO_KIND + "/"
	os.MkdirAll(out, 0744)
	gifOut := out + ALGO_KIND + ".gif"

	f, err := os.Create(gifOut)
	check(err)
	defer f.Close()
	gif.EncodeAll(f, &gif.GIF{
		Image: images,
		Delay: delays,
	})
	if SAVE_FRAMES {
		for i, rgb := range images {
			dst := out + int2str(i) + ".jpg"
			f, err := os.Create(dst)
			check(err)
			jpeg.Encode(f, rgb, nil)
		}
	}
}

func int2str(num int) string {
	var res string
	len := 0
	for num > 0 {
		digit := num % 10
		res = strconv.Itoa(digit) + res
		num /= 10
		len++
	}

	sb := strings.Builder{}
	for i := 0; i < 3-len; i++ {
		sb.WriteByte('0')
	}
	sb.WriteString(res)

	return sb.String()
}

func check(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

const (
	Comparing State = 0
	Swapping  State = 1
	Done      State = 2
)

var palette = []color.Color{
	color.RGBA{0x00, 0x00, 0x00, 0xff}, color.RGBA{0x00, 0x00, 0xff, 0xff},
	color.RGBA{0x00, 0xff, 0x00, 0xff}, color.RGBA{0x00, 0xff, 0xff, 0xff},
	color.RGBA{0xff, 0x00, 0x00, 0xff}, color.RGBA{0xff, 0x00, 0xff, 0xff},
	color.RGBA{0xff, 0xff, 0x00, 0xff}, color.RGBA{0xff, 0xff, 0xff, 0xff},
}

var SCALE float32

var images []*image.Paletted
var delays []int
