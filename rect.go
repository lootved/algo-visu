package main

import (
	"image"
	"image/color"
)

const RECT_OFFSET = 10
const RECT_MIN_HEIGHT = 10

var RECT_HEIGHT_OFFSET int
var RECT_WIDTH int

type Rect struct {
	x1, y1, x2, y2 int
	colorr         color.RGBA
}

func (r *Rect) Set(x, y, w, h int) {
	//used like a constructor
	r.x1 = x
	r.x2 = x + w
	r.y1 = y
	r.y2 = y + h
	r.colorr.A = 255
}

func (r *Rect) SetX(x int) *Rect {
	r.x2 = r.x2 - r.x1 + x
	r.x1 = x
	return r
}

func (r *Rect) SetColor(R, G, B uint8) *Rect {
	r.colorr.R = R
	r.colorr.G = G
	r.colorr.B = B
	return r
}

func (r *Rect) Draw(img *image.Paletted, offset_y int) {
	for x := r.x1; x <= r.x2; x++ {
		for y := r.y1; y <= r.y2; y++ {
			img.Set(x, HEIGHT-offset_y-y, r.colorr)
		}
	}
}

func (r *Rect) Contains(x, y int) bool {
	return (x >= r.x1 && x <= r.x2 && y >= r.y1 && y <= r.y2)
}
