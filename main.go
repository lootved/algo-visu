package main

import (
	"flag"
	"fmt"
	"os"
)

const WIDTH, HEIGHT int = 1200, 800
const STEPS = 3

type State uint8

var nums []int

var rects []*Rect
var ALGO_KIND string = "binsearch"
var GIF_DELAY = 20
var SAVE_FRAMES = false

var MIN_VAL int
var MAX_VAL int

func main() {

	nums = []int{8, 5, 4, 6, 5, 8, 10, 15, 20, 12, 13, 15, 8, 7, 9}
	n := len(nums)
	dsc := false
	flag.BoolVar(&dsc, "dsc", dsc, "sort in descending order")
	flag.BoolVar(&SAVE_FRAMES, "s", SAVE_FRAMES, "save all frames")
	flag.StringVar(&ALGO_KIND, "k", ALGO_KIND, "algorithm to visualize")
	flag.IntVar(&GIF_DELAY, "d", GIF_DELAY, "delays between gif frames")
	flag.Parse()

	if dsc {
		compare_fn = _descending
	}

	sortFn, exists := name_to_algo[ALGO_KIND]
	if !exists {
		fmt.Println("invalid algo parameter provided, acceptable values:")
		for k := range name_to_algo {
			fmt.Print(k + " ")
		}
		fmt.Println()
		os.Exit(1)
	}

	MIN_VAL = nums[0]
	MAX_VAL = nums[0]
	for i := 1; i < n; i++ {
		MIN_VAL = min(MIN_VAL, nums[i])
		MAX_VAL = max(MAX_VAL, nums[i])
	}

	rects = make([]*Rect, n)
	isInCorrectPos = make([]bool, n)

	RECT_WIDTH = (WIDTH-2*RECT_OFFSET)/n - RECT_OFFSET
	RECT_WIDTH = RECT_WIDTH * 2 / 3

	SCALE = (float32)(4*HEIGHT/5-RECT_OFFSET) / float32(MAX_VAL)
	RECT_HEIGHT_OFFSET = HEIGHT / 3
	for i := 0; i < n; i++ {
		rects[i] = &Rect{}
	}
	sortFn(nums)
	save_img()
}
