#  Algorithm-Visualiser

Visualize common algorithms and generate jpg/gif .

sorting:
  - quick-sort
  - insertion-sort
  - bubble-sort

searching:
  - binary search